// 1.

const firstQuestion = () => {
    let result = 0
    for (let i = 0; i < 10; i++) {
        if (i % 2 === 1) {
            result = result + (i * 2)
        }
        else {
            result += i
        }
    }

    // result = 70
    console.log(result);
}

firstQuestion()

////////////////////

// 2.
// Ans. Prime Number

////////////////////

// 3.
/*
3.1
Ans. Kim Odd , Susan Ray
3.2
Ans. Laura Larson เพราะ ประสิทธิภาพในการทำงานเยอะที่สุด
3.3
Ans. Laura Larson เพราะ เฉลี่ยค่าจ้างต่อวัน(คิดจากทำงาน20วันต่อเดือน)แล้วLaura ค่าแรงถูกที่สุด200บาทต่อวัน แต่ได้ประสิทธิภาพมากที่สุด
3.4
Ans. 
SELECT name,lastname 
FROM employee
INNER JOIN employee_work
ON age>25 AND perf>80
 */

// 4.
const fibonacci = (n) => {
    if (n < 2) {
        return n
    }
    return fibonacci(n - 1) + fibonacci(n - 2)
}

const result = fibonacci(3)
console.log(result);

// 5.
const isPrimeNumber = (n) => {
    for (var i = 2; i < n; i++) {
        if (n % i === 0) {
            return false;
        }
    }
    return true;
}
const sumPrimeNumber = (input) => {
    let result = 0
    if (input === 2) {
        result = 2
    }
    for (let i = 2; i < input; i++) {
        if (isPrimeNumber(i)) {
            result += i
        }
    }
    console.log(result);
}

sumPrimeNumber(15)