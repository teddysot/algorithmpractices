

const greetingBtn = $(".greeting-button")
greetingBtn.click(() => {
    const text = greetingBtn.text()
    alert(text);
});


const renderQuestion = () => {
    let content = `<div id="question-section">`

    myQuestions.map((question, idx) => {
        content += `<div class="question">${question.question}`
        question.answers.forEach((answer) => {
            content += `<input type="radio" id="q${idx}-${answer.trim()}" name="q${idx}-${answer.trim()}">
                        <label for="q${idx}-${answer.trim()}">${answer}</label><br>`
        })
        content += `</div>`
    })
    $(document.body).append(content);
}

const checkAnswer = () => {
    let result = new Array(myQuestions.length).fill(false)

    myQuestions.map((question, idx) => {
        question.answers.forEach((answer) => {
            const userAns = $(`input[name="q${idx}-${answer.trim()}"]:checked`).val();
            if (userAns.length > 0) {
                if (question.correctAnswer === question.answers.indexOf(`${userAns}`)) {
                    result[idx] = true
                }
            }
        })
    })
}

const checkUserAnswerAllQuestion = () => {
    let result = new Array(myQuestions.length).fill(false)

    myQuestions.map((question, idx) => {
        question.answers.forEach((answer) => {
            const userAns = $(`input[name="q${idx}-${answer.trim()}"]:checked`).val();
            if (userAns.length > 0) {
                result[idx] = true
            }
        })

        if (result[idx] === false) {
            $(`#question-section`).append(`<div>Please answer all questions</div>`)
        }
    })
}