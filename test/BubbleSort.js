const bubbleSort = (nums) => {
    let swapped = false
    do {
        swapped = false
        for (let i = 0; i < nums.length; i++) {

            if (nums[i] > nums[i + 1]) {
                const temp = nums[i]
                nums[i] = nums[i + 1]
                nums[i + 1] = temp
                swapped = true
            }
        }
    } while (swapped)
    return nums
}

let nums = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

nums = bubbleSort(nums)

console.log(nums);