const XXH = require('xxhashjs')

const h1 = (string) => {
    Math.abs(XXH.h32(0xABCD).update(string).digest().toNumber() % 100)
}
const h2 = (string) => {
    Math.abs(XXH.h32(0x1234).update(string).digest().toNumber() % 100)
}
const h3 = (string) => {
    Math.abs(XXH.h32(0x6789).update(string).digest().toNumber() % 100)
}

class BloomFilter {
    _array = new Array(100).fill(0)
    add(string) {
        this._array[h1(string)] = 1
        this._array[h2(string)] = 1
        this._array[h3(string)] = 1
    }

    contains(string) {
        return !!(this._array[h1(string)] && this._array[h2(string)] && this._array[h3(string)])
    }
}

let bf = new BloomFilter()

console.log(bf.contains("Brian"));
console.log(bf.contains("Sarah"));
console.log(bf.contains("Simona"));

bf.add("Brian")
console.log(bf.contains("Brian"));