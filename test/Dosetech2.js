// 1. จงเขยนโปรแกรมเรยงลดบตวเลข
// ตัวอยาง Input = [10,5,11,20,4,8], output = [4,5,8,10,11,20]

const swap = (items, leftIndex, rightIndex) => {
    const temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
}

const partition = (items, left, right) => {
    const pivot = items[Math.floor((right + left) / 2)]
    let i = left
    let j = right
    while (i <= j) {
        while (items[i] < pivot) {
            i++
        }
        while (items[j] > pivot) {
            j--
        }
        if (i <= j) {
            swap(items, i, j)
            i++
            j--
        }
    }
    return i;
}

const quickSort = (items, left, right) => {
    let index = -1;
    if (items.length > 1) {
        index = partition(items, left, right);
        if (left < index - 1) {
            quickSort(items, left, index - 1);
        }
        if (index < right) {
            quickSort(items, index, right);
        }
    }
    return items;
}

const unsortedArray = [10, 5, 11, 20, 4, 8]

let sortedArray = quickSort(unsortedArray, 0, unsortedArray.length - 1)

console.log(sortedArray);

// 2.

/*
Input
product
promotion
Output
products[]
*/


const products = [
    {
        ProductID: 1,
        Name: "Computer Notebook",
        Price: 15000,
        Qty: 2,
        AfterDiscountPrice: 0
    }
]

const promotions = [
    {
        ID: 1,
        Name: "Discount 15%",
        Discount: 15,
        DiscountType: "Percent",
        ProductID: [1, 2, 3, 4, 5]
    }
]

const getPromotion = (productID) => {
    const result = promotions.find((promo) => {
        const result = promo.ProductID.indexOf(productID)
        console.log({ result });
        if (result > -1) {
            return promo
        }
        else {
            return null
        }
    })

    if (result) {
        return { Discount: result.Discount, DiscountType: result.DiscountType }
    }
    else {
        return null
    }
}

const finalSale = () => {

    products.map((product) => {
        const promotion = getPromotion(product.ProductID)
        if (promotion) {
            switch (promotion.DiscountType) {
                case "Percent":
                    product.AfterDiscountPrice = product.Price * (promotion.Discount / 100)
                case "Baht":
                    product.AfterDiscountPrice = product.Price - promotion.Discount
                default:
                    break;
            }
        }
        return product
    })

}

finalSale()

console.log(products);