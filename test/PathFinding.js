const fourByFour = [
    [2, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 2]
]

const sixBySix = [
    [0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 2, 0, 0, 0]
];

const eightByEight = [
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 2, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 1, 2]
];

const fifteenByFifteen = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0,],
    [0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0,],
    [0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0,],
    [0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0,],
    [0, 0, 1, 0, 1, 0, 1, 1, 2, 1, 0, 1, 0, 1, 0,],
    [0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0,],
    [0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0,],
    [0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0,],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0,],
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0,],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
]

const byEachOther = [
    [0, 0, 0, 0, 0],
    [0, 2, 2, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1],
    [0, 0, 0, 0, 0],
];

const impossible = [
    [0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0],
    [0, 0, 1, 1, 1],
    [1, 1, 1, 0, 0],
    [0, 0, 0, 0, 2],
];

const logMaze = (maze) => {
    console.log('================');
    let header = 'XX | '
    let subheader = '-----'
    for (let i = 0; i < maze[0].length; i++) {
        const num = i >= 10 ? i : '0' + i;
        header += `${num} `
        subheader += '---'
    }
    console.log(header);
    console.log(subheader);
    maze.forEach((row, i) => {
        const num = i >= 10 ? i : '0' + i;
        let buffer = `${num} | `

        row.forEach((item) => {
            if (item.closed) {
                buffer += 'XX '
            } else if (item.openedBy === NO_ONE) {
                buffer += '•• '
            } else {
                buffer += (item.length >= 10 ? item.length : '0' + item.length) + ' '
            }
        })

        console.log(buffer);
    })
}

const NO_ONE = 0;
const BY_A = 1;
const BY_B = 2;

const findShortestPathLength = (maze, [xA, yA], [xB, yB]) => {
    const queue = [];
    const visited = maze.map((row, y) =>
        row.map((origin, x) => ({
            closed: origin === 1,
            length: 0,
            openedBy: NO_ONE,
            x,
            y
        }))
    );
    visited[yA][xA].openedBy = BY_A;
    visited[yB][xB].openedBy = BY_B;
    logMaze(visited);

    let aQueue = [visited[yA][xA]];
    let bQueue = [visited[yB][xB]];
    let iteration = 0;

    // if one runs out, there's no path
    while (aQueue.length && bQueue.length) {
        iteration++;
        const aNeighbors = aQueue.reduce((acc, neighbor) => acc.concat(getNeighbors(visited, neighbor.x, neighbor.y)), [])
        aQueue = [];
        for (let i = 0; i < aNeighbors.length; i++) {
            const neighbor = aNeighbors[i];
            if (neighbor.openedBy === BY_B) {
                return neighbor.length + iteration;
            } else if (neighbor.openedBy === NO_ONE) {
                neighbor.length = iteration;
                neighbor.openedBy = BY_A;
                aQueue.push(neighbor);
            }
        }

        const bNeighbors = bQueue.reduce((acc, neighbor) => acc.concat(getNeighbors(visited, neighbor.x, neighbor.y)), [])
        bQueue = [];
        for (let i = 0; i < bNeighbors.length; i++) {
            const neighbor = bNeighbors[i];
            if (neighbor.openedBy === BY_A) {
                return neighbor.length + iteration;
            } else if (neighbor.openedBy === NO_ONE) {
                neighbor.length = iteration;
                neighbor.openedBy = BY_B;
                bQueue.push(neighbor);
            }
        }
        logMaze(visited);
    }
    return -1;
};

const getNeighbors = (visited, x, y) => {
    const neighbors = []

    // Left
    if (y - 1 > 0 && !visited[y - 1].closed) {
        neighbors.push(visited[y - 1][x])
    }

    // Right
    if (y + 1 < visited[0].length && !visited[y - 1][x].closed) {
        neighbors.push(visited[y + 1][x])
    }

    // Down
    if (x + 1 < visited[0].length && !visited[y][x + 1].closed) {
        neighbors.push(visited[y][x + 1])
    }

    // Up
    if (x - 1 > 0 && !visited[y][x - 1].closed) {
        neighbors.push(visited[y][x - 1])
    }

    return neighbors
}

console.log(findShortestPathLength(sixBySix, [1, 1], [2, 5]))
console.log(findShortestPathLength(byEachOther, [1, 1], [2, 1]))
console.log(findShortestPathLength(eightByEight, [1, 7], [7, 7]))
console.log(findShortestPathLength(fifteenByFifteen, [1, 1], [8, 8]))
console.log(findShortestPathLength(impossible, [1, 1], [4, 4]))
