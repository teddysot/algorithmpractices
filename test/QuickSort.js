const quickSort = (nums) => {
    if (nums.length <= 1) return nums;

    const pivot = nums.pop()
    const left = []
    const right = []

    for (let i = 0; i < nums.length - 1; i++) {
        if (nums[i] < pivot) {
            left.push(nums[i])
        }
        else {
            right.push(nums[i])
        }
    }

    return [...quickSort(left), pivot, ...quickSort(right)]
}

let nums = [10, 2, 8, 7, 6, 5, 4, 3, 9, 1]

nums = quickSort(nums)

console.log(nums);