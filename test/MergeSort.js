const mergeSort = (nums) => {
    if (nums.length < 2) {
        return nums
    }

    const length = nums.length
    const middle = Math.floor(length / 2)
    const left = nums.slice(0, middle)
    const right = nums.slice(middle, length)

    const sortedLeft = mergeSort(left);
    const sortedRight = mergeSort(right);

    return stitch(sortedLeft, sortedRight)
}

const stitch = (left, right) => {
    const results = []

    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            results.push(left.shift())
        }
        else {
            results.push(right.shift())
        }
    }

    return [...results, ...left, ...right] // Join array
}

let nums = [10, 2, 8, 7, 6, 5, 4, 3, 9, 1]

nums = mergeSort(nums)

console.log(nums);