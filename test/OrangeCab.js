// 1. Fibonacci Sequence: Write a function fib that return the value of n-th order of fibonacci sequence.
const fib = (n) => {
    if (n < 2) {
        return n
    }
    return fib(n - 1) + fib(n - 2)
}

// 2. Array shift: Write a function shift that shifts the elements of array to left or right by n elements in an infinite loop.
const shift = (arr, dir, index) => {
    dir = dir.toLowerCase()
    switch (dir) {
        case "left":
            arr.splice(index, 0, arr.splice(index - 1, 1)[0])
            return arr
        case "right":
            arr.splice(index, 0, arr.splice(index + 1, 1)[0])
            return arr

        default:
            return "Invalid Option"
    }
}

const tempArr = [1, 2, 3]
console.log(shift(tempArr, "right", 1))

// 3. Second max: Write a function secondMax that receive an array of number. 
// The function will return the second maximum value of the array. 
// If there is no second max, return max instead. If an array is empty, throw and error.

const secondMax = (arr) => {
    if (!arr || arr.length < 1) {
        console.log("Error!");
        return
    }
    arr = new Set(arr)

    arr = [...arr]

    if (arr.length === 1) {
        console.log(arr[0]);
        return
    }
    let max = [0, 0]

    for (const num of arr) {

        if (num > max[1]) {
            const temp = max[1]
            max[0] = temp
            max[1] = num
        }
    }

    console.log(max[0]);
}

const secondMaxArr = []

secondMax(secondMaxArr)

// 4.FizzBuzz...But: You may heard FizzBuzz task. 
// Here we have the same rule. You will write a function fizzBuzz that receive a single parameter 
// it will return the value base on these rule.

// If the input is divisable by 3, return 'Fizz'
// If the input is divisable by 5, return 'Buzz'
// If the input is divisable by both 3 and 5, return 'FizzBuzz'

const fizzBuzz = (n) => {
    if (n % 3 === 0 && n % 5 === 0) {
        console.log("FizzBuzz");
        return
    }
    else if (n % 3 === 0) {
        console.log("Fizz");
        return
    }
    else if (n % 5 === 0) {
        console.log("Buzz");
        return
    }
}

fizzBuzz(45)