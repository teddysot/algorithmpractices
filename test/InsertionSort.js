const insertionSort = (nums) => {
    for (let i = 1; i < nums.length; i++) {
        for (let j = 0; j < i; j++) {
            if (nums[i] < nums[j]) {
                const spliced = nums.splice(i, 1) // Remove one element
                nums.splice(j, 0, spliced[0]) // Add Front
            }
        }
    }
    return nums
}

let nums = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]

nums = insertionSort(nums)

console.log(nums);